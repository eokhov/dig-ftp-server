package ftp

import (
	"fmt"
	"log"
)

const (
	status150 = ""
	status200 = "200 Command ok."
	status221 = "221 Service closing control connection."
	status226 = ""
	status230 = ""
	status425 = ""
	status426 = ""
	status501 = ""
	status502 = "502 Command not implemented"
	status504 = ""
	status550 = ""
)

func (conn *Conn) respond(s string) {
	log.Print(">>", s)
	_, err := fmt.Fprint(conn.conn, s, conn.EOL())

	if err != nil {
		log.Print(err)
	}
}

func (conn *Conn) EOL() string {
	switch conn.dataType {
	case ascii:
		return "\r\n"
	case binary:
		return "\n"
	default:
		return "\n"
	}
}
