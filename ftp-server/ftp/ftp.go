package ftp

import (
	"bufio"
	"log"
	"strings"
)

func Serve(conn *Conn) {
	conn.respond(status200)

	s := bufio.NewScanner(conn.conn)

	for s.Scan() {
		input := strings.Fields(s.Text())
		if len(input) == 0 {
			continue
		}
		command, args := input[0], input[1:]
		log.Printf("<< %s %v", command, args)

		switch command {
		case "CWD":
			conn.cwd(args)
		case "LIST":
			conn.list(args)
		case "PORT":
			conn.port(args)
		case "USER":
			conn.user(args)
		case "QUIT":
			conn.respond(status221)
		case "RETR":
			conn.retr(args)
		case "TYPE":
			conn.setDataType(args)
		default:
			conn.respond(status502)
		}
	}

	if s.Err() != nil {
		log.Print(s.Err())
	}
}
