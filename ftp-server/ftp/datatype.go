package ftp

type dataType int

const (
	ascii dataType = iota
	binary
)

func (conn *Conn) setDataType(args []string) {
	if len(args) == 0 {
		conn.respond(status501)
	}

	switch args[0] {
	case "A":
		conn.dataType = ascii
	case "I":
		conn.dataType = binary
	default:
		conn.respond(status504)
		return
	}
	conn.respond(status200)
}
