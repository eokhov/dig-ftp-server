package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"path/filepath"

	"gitlab.com/dive-into-golang/ftp-server/ftp"
)

var port int
var rootDir string

func init() {
	flag.IntVar(&port, "port", 8080, "port number")
	flag.StringVar(&rootDir, "rootDir", "public", "root directory")
	flag.Parse()
}

func handleConnection(conn net.Conn) {
	defer conn.Close()
	absPath, err := filepath.Abs(rootDir)

	if err != nil {
		log.Fatal(err)
	}
	ftp.Serve(ftp.NewConn(conn, absPath))
}

func main() {
	server := fmt.Sprintf("%d", port)
	listener, err := net.Listen("tcp", server)

	if err != nil {
		log.Fatal(err)
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Print(err)
			continue
		}
		go handleConnection(conn)
	}
}
